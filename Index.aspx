﻿<%@ Page language="C#" CodeBehind="Index.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GenericType sample</title>
</head>
<body>
<form runat="server">
    <asp:ObjectDataSource
        ID="EmployeesObjectDataSource"
        runat="server"
        TypeName="GenericTypeSample.Index"
        DataObjectTypeName="System.Collections.Generic.KeyValuePair<int, int>"
        SelectMethod="GetDataSource">
    </asp:ObjectDataSource>

    <table>
        <tr>
            <td>
                <asp:GridView ID="EmployeesGridView"
                              DataSourceID="EmployeesObjectDataSource"
                              RunAt="server">
                </asp:GridView>
            </td>
        </tr>
    </table>
</form>
</body>
</html>