<%@ Page language="C#" CodeBehind="Sample.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GenericType sample</title>
</head>
<body>
<form runat="server">
    <asp:ObjectDataSource
        ID="SampleObjectDataSource"
        runat="server"
        TypeName="GenericTypeSample.Index"
        DataObjectTypeName="GenericTypeSample.SampleDataObject"
        SelectMethod="GetDataSource">
    </asp:ObjectDataSource>

    <table>
        <tr>
            <td>
                <asp:GridView ID="EmployeesGridView"
                              DataSourceID="SampleObjectDataSource"
                              RunAt="server">
                </asp:GridView>
            </td>
        </tr>
    </table>
</form>
</body>
</html>