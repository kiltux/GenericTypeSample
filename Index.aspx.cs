﻿using System.Collections.Generic;

namespace GenericTypeSample
{
    public class Index
    {
        public Dictionary<int, int> GetDataSource() => new Dictionary<int, int> { [1] = 1, [2] = 2 };
    }
}