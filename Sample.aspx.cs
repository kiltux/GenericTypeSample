using System.Collections.Generic;
using System.Linq;

namespace GenericTypeSample
{
    public class Sample
    {
        public IEnumerable<SampleDataObject> GetDataSource() => Enumerable.Range(1, 5).Select(i => new SampleDataObject(i));
    }

    public class SampleDataObject
    {
        public SampleDataObject(int number)
        {
            this.Number = number;
        }

        public int Number { get; }
    }
}